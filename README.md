# Plan for automation of The Password Game
1. Program-website interface (Week 1)
2. Automatic captcha solving (Week 2)
3. Chess AI integration (Week 3)
4. Youtube API (Week 4)
5. List things that must be done by humans (Week 5)
6. Integration and testing (Week 6)

# Procedure for The Password Game for July 1st
> Warning: Wordle spoilers

> Prerequisite: Chessvision AI Extension https://chessvision.ai/

Rule 1 - 9: Paste `0🌔🥚 55555 AmarchpepsiI XXXV xngxc ndyfe bbymy dbfen`

Rule 10: Refresh until you get `xngxc`, `ndyfe`, `bbymy`, or `dbfen`
> Remove the exra captchas

Rule 11: BLEEP (Bugged, anything fulfills this rule)

Rule 12: Already fulfilled

Rule 13: Already fulfilled

Rule 14: ...

Rule 15: Already fulfilled

Rule 16: Use chessvision to scan the board

Rule 17: Already fufilled

Rule 18: https://ptable.com

Rule 19: ...

Rule 20: **FIRE**